import React from "react";
import App from "./App";
import { screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { renderWithRouter } from "./testUtils";

describe("navigating", () => {
  it("should navigate to dashboard", () => {
    renderWithRouter(<App />);
    expect(screen.getByText(/full secure login/i)).toBeInTheDocument();
    const loginBtn = screen.getByTestId("login-btn");
    const leftClick = { button: 0 };
    userEvent.click(loginBtn, leftClick);
  });
});
