export {
  default,
  actionCreators,
  actionTypes,
  selector,
  NAME,
} from "./historyList.js";
