import reducer, { actionTypes, actionCreators } from "../";

const initialState = {
  historyList: [],
  isLoading: false,
  err: {},
};

describe("historyList actionCreators", () => {
  it("should create an action to add new history item", () => {
    const item = {};
    const expectedAction = {
      type: actionTypes.HISTORY_ADD,
      payload: item,
    };
    expect(actionCreators.historyAdd(item)).toEqual(expectedAction);
  });

  it("should create an action to clean all history items", () => {
    const expectedAction = {
      type: actionTypes.HISTORY_CLEAN,
    };
    expect(actionCreators.historyClean()).toEqual(expectedAction);
  });

  it("should create an action to delete a history item", () => {
    const item = {};
    const expectedAction = {
      type: actionTypes.HISTORY_DELETE,
      payload: item,
    };
    expect(actionCreators.historyDelete(item)).toEqual(expectedAction);
  });
});

describe("historyList reducers", () => {
  it("should return the initial state", () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it("should return the state with a new item", () => {
    const item = {
      provincia: "new provincia",
    };
    expect(reducer(initialState, actionCreators.historyAdd(item))).toEqual({
      ...initialState,
      historyList: [...initialState.historyList, item],
    });
  });

  it("should return the state with one item deleted", () => {
    const item = {
      provincia: "item to delete",
    };
    const state = {
      ...initialState,
      historyList: [...initialState.historyList, item],
    };
    expect(reducer(state, actionCreators.historyDelete(item))).toEqual(
      initialState
    );
  });

  it("should return the state with the error object updated", () => {
    const error = { message: "error" };
    expect(reducer(initialState, actionCreators.historyFailed(error))).toEqual({
      ...initialState,
      err: error,
    });
  });
});
