import { createStructuredSelector } from "reselect";

export const NAME = "history";

//Action Types
const HISTORY_CLEAN = "[history]/DETAIL_CLEAN";
const HISTORY_FAILED = "[history]/DETAIL_FAILED";
const HISTORY_ADD = "[history]/DETAIL_ADD";
const HISTORY_DELETE = "[history]/DETAIL_DELETE";

//Initial State
const initialState = {
  historyList: [],
  isLoading: false,
  err: {},
};

//Reducer
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case actionTypes.HISTORY_CLEAN:
      return { ...state, historyList: [], isLoading: false };

    case actionTypes.HISTORY_FAILED:
      return { ...state, err: action.payload };

    case actionTypes.HISTORY_ADD:
      return {
        ...state,
        historyList: [...state.historyList, action.payload],
        isLoading: false,
      };

    case actionTypes.HISTORY_DELETE:
      const updatedHistory = state.historyList.filter(function (item) {
        return action.payload !== item;
      });

      return {
        ...state,
        historyList: [...updatedHistory],
      };

    default:
      return state;
  }
}

//Action creators
function historyClean() {
  return { type: actionTypes.HISTORY_CLEAN };
}

function historyAdd(payload) {
  return { type: actionTypes.HISTORY_ADD, payload };
}

function historyDelete(payload) {
  return { type: actionTypes.HISTORY_DELETE, payload };
}

function historyFailed(payload) {
  return { type: actionTypes.HISTORY_FAILED, payload };
}

export const actionCreators = {
  historyClean,
  historyAdd,
  historyDelete,
  historyFailed,
};

//Action types
export const actionTypes = {
  HISTORY_ADD,
  HISTORY_CLEAN,
  HISTORY_FAILED,
  HISTORY_DELETE,
};

//Selector
const historyState = (state) => state[NAME];

export const selector = createStructuredSelector({
  historyState,
});
