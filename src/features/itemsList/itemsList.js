import { createStructuredSelector } from "reselect";

export const NAME = "list";

//Action Types
const DETAIL_TRY = "[list]/DETAIL_TRY";
const DETAIL_CLEAN = "[list]/DETAIL_CLEAN";
const DETAIL_FAILED = "[list]/DETAIL_FAILED";
const DETAIL_ADD_MUNICIPIO = "[list]/DETAIL_ADD_MUNICIPIO";
const DETAIL_ADD_PROVINCIA = "[list]/DETAIL_ADD_PROVINCIA";
const DETAIL_DELETE_MUNICIPIO = "[list]/DETAIL_DELETE_MUNICIPIO";

//Initial State
const initialState = {
  detailList: [],
  isLoading: false,
  err: {},
};

//Reducer
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case actionTypes.DETAIL_TRY:
      return {
        ...state,
        isLoading: true,
      };
    case actionTypes.DETAIL_CLEAN:
      return { ...state, detailList: [], isLoading: false };
    case actionTypes.DETAIL_FAILED:
      return { ...state, err: action.payload };
    case actionTypes.DETAIL_ADD_PROVINCIA:
      //add new provincia & clean unused municipios data
      return {
        ...state,
        detailList: [
          ...state.detailList,
          { ...action.payload, municipios: [] },
        ],
        isLoading: false,
      };

    case actionTypes.DETAIL_ADD_MUNICIPIO:
      //find provincia object of current municipio(action.payload) and add it to municipios array
      const { municipio } = action.payload;
      const pr = state.detailList.filter(function (item) {
        return municipio.CODPROV === item.codprov;
      });
      const otherPrs = state.detailList.filter(function (item) {
        return municipio.CODPROV !== item.codprov;
      });
      const updatedPr = {
        ...pr[0],
        municipios: [...pr[0].municipios, action.payload],
      };
      return {
        ...state,
        detailList: [...otherPrs, updatedPr],
      };

    case actionTypes.DETAIL_DELETE_MUNICIPIO:
      const { CODPROV, CODIGOINE } = action.payload.municipio;

      const provincia = state.detailList.filter(function (item) {
        return CODPROV === item.codprov;
      });

      const updatedMunicipios = provincia[0].municipios.filter(function (
        municipio
      ) {
        return CODIGOINE !== municipio.municipio.CODIGOINE;
      });

      const updatedProvincia = {
        ...provincia[0],
        municipios: updatedMunicipios,
      };

      const otherProvincias = state.detailList.filter(function (item) {
        return CODPROV !== item.codprov;
      });

      return {
        ...state,
        detailList: [...otherProvincias, updatedProvincia],
      };

    default:
      return state;
  }
}

//Action creators
function list(payload) {
  return { type: actionTypes.DETAIL_TRY, payload };
}

function detailClean() {
  return { type: actionTypes.DETAIL_CLEAN };
}

function detailFailed(payload) {
  return { type: actionTypes.DETAIL_FAILED, payload };
}

function detailAddMunicipio(payload) {
  return { type: actionTypes.DETAIL_ADD_MUNICIPIO, payload };
}

function detailAddProvincia(payload) {
  return { type: actionTypes.DETAIL_ADD_PROVINCIA, payload };
}

function detailDeleteMunicipio(payload) {
  return { type: actionTypes.DETAIL_DELETE_MUNICIPIO, payload };
}

export const actionCreators = {
  list,
  detailClean,
  detailFailed,
  detailAddMunicipio,
  detailAddProvincia,
  detailDeleteMunicipio,
};

//Action types
export const actionTypes = {
  DETAIL_TRY,
  DETAIL_ADD_MUNICIPIO,
  DETAIL_ADD_PROVINCIA,
  DETAIL_CLEAN,
  DETAIL_FAILED,
  DETAIL_DELETE_MUNICIPIO,
};

//Selector
const listState = (state) => state[NAME];

export const selector = createStructuredSelector({
  listState,
});
