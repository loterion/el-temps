import reducer, { actionTypes, actionCreators } from "../";

const initialState = {
  detailList: [],
  isLoading: false,
  err: {},
};

describe("itemsList actionCreators", () => {
  it("should create an action to clean detailList", () => {
    const expectedAction = {
      type: actionTypes.DETAIL_CLEAN,
    };
    expect(actionCreators.detailClean()).toEqual(expectedAction);
  });

  it("should create an action to update err object", () => {
    const item = { err: "error" };
    const expectedAction = {
      type: actionTypes.DETAIL_FAILED,
      payload: item,
    };
    expect(actionCreators.detailFailed(item)).toEqual(expectedAction);
  });

  it("should create an action to add new municipio", () => {
    const item = { item: "municipio" };
    const expectedAction = {
      type: actionTypes.DETAIL_ADD_MUNICIPIO,
      payload: item,
    };
    expect(actionCreators.detailAddMunicipio(item)).toEqual(expectedAction);
  });

  it("should create an action to add new provincia", () => {
    const item = { item: "provincia" };
    const expectedAction = {
      type: actionTypes.DETAIL_ADD_PROVINCIA,
      payload: item,
    };
    expect(actionCreators.detailAddProvincia(item)).toEqual(expectedAction);
  });

  it("should create an action to delete one municipio", () => {
    const item = { item: "municipio" };
    const expectedAction = {
      type: actionTypes.DETAIL_DELETE_MUNICIPIO,
      payload: item,
    };
    expect(actionCreators.detailDeleteMunicipio(item)).toEqual(expectedAction);
  });
});

describe("itemsList reducer", () => {
  it("should return the initial state", () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it("should return the state with the detailList empty", () => {
    const state = {
      ...initialState,
      detailList: [{ item: "item1" }, { item: "item2" }],
    };
    expect(reducer(state, actionCreators.detailClean())).toEqual(initialState);
  });

  it("should return the state with a new provincia in the detailList", () => {
    const provincia = { provincia: "new provincia", municipios: [] };
    const updatedState = { ...initialState, detailList: [provincia] };
    expect(
      reducer(initialState, actionCreators.detailAddProvincia(provincia))
    ).toEqual(updatedState);
  });

  it("should return the state with a new municipio", () => {
    const newMunicipio = {
      municipio: { NOMBRE: "new municipio", CODPROV: "500" },
    };
    const state = {
      ...initialState,
      detailList: [
        { provincia: "provincia2", codprov: "502", municipios: [] },
        { provincia: "provincia1", codprov: "500", municipios: [] },
      ],
    };
    const expectedState = {
      ...initialState,
      detailList: [
        { provincia: "provincia2", codprov: "502", municipios: [] },
        { provincia: "provincia1", codprov: "500", municipios: [newMunicipio] },
      ],
    };
    expect(
      reducer(state, actionCreators.detailAddMunicipio(newMunicipio))
    ).toEqual(expectedState);
  });
});
