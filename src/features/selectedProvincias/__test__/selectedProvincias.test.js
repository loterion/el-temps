import reducer, { actionCreators, actionTypes } from "../";

describe("selectedProvincias actionCreators", () => {
  it("should create an action to set selectedProvincias", () => {
    const item = [{ codigo: "00", label: "test provincia" }];
    const expectedAction = {
      type: actionTypes.SET_PROVINCIAS,
      payload: item,
    };
    expect(actionCreators.setProvincias(item)).toEqual(expectedAction);
  });
});

describe("selectedProvincias reducers", () => {
  it("should return the initial state", () => {
    expect(reducer(undefined, {})).toEqual([]);
  });

  it("should return the updated state", () => {
    const newState = [
      {
        codigo: "00",
        label: "Item1",
      },
      {
        codigo: "01",
        label: "Item2",
      },
    ];
    expect(reducer([], actionCreators.setProvincias(newState))).toEqual([
      ...newState,
    ]);
  });
});
