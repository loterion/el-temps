import { createStructuredSelector } from "reselect";

export const NAME = "selected_provincias";

//Action Types
const SET_PROVINCIAS = "[selected_provincias]/SET_PROVINCIAS";

//Initial State
const initialState = [];

//Reducer
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case actionTypes.SET_PROVINCIAS:
      return action.payload;
    default:
      return state;
  }
}

//Action creators
function setProvincias(payload) {
  return { type: actionTypes.SET_PROVINCIAS, payload };
}

export const actionCreators = {
  setProvincias,
};

export const actionTypes = {
  SET_PROVINCIAS,
};

const selectedProvincias = (state) => state[NAME];

export const selector = createStructuredSelector({
  selectedProvincias,
});
