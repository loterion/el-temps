import React from "react";
import {
  EuiButton,
  EuiFieldText,
  EuiForm,
  EuiFormRow,
  EuiTitle,
  EuiFlexGroup,
  EuiSpacer,
} from "@elastic/eui";

export default function Register() {
  return (
    <>
      <EuiSpacer size="xxl" />
      <EuiTitle size="l">
        <h1 style={{ textAlign: "center" }}>Nuevo usuario</h1>
      </EuiTitle>
      <EuiSpacer size="xxl" />
      <EuiFlexGroup justifyContent="spaceAround">
        <EuiForm component="form">
          <EuiFormRow label="Username">
            <EuiFieldText name="username" />
          </EuiFormRow>
          <EuiSpacer size="s" />
          <EuiFormRow label="Password">
            <EuiFieldText name="password" />
          </EuiFormRow>
          <EuiSpacer size="xl" />
          <EuiButton type="submit" fill>
            Registrar
          </EuiButton>
        </EuiForm>
      </EuiFlexGroup>
      <EuiSpacer size="xxl" />
    </>
  );
}
