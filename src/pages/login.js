import React, { useState } from "react";
import { EuiButton, EuiTitle, EuiFlexGroup, EuiSpacer } from "@elastic/eui";
import { Redirect } from "react-router-dom";
import { Lottie } from "@crello/react-lottie";
import nublado from "../lottie/nublado.json";

export default function Login({ fakeAuth }) {
  const [redirectToReferrer, setRedirectToReferrer] = useState(false);

  const login = () => {
    fakeAuth.authenticate(() => {
      setRedirectToReferrer(true);
    });
  };

  if (redirectToReferrer) {
    return <Redirect to="/" />;
  }

  //Lottie stuff
  const lottieOptions = {
    loop: true,
    autoplay: true,
    animationData: nublado,
    renderSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return (
    <>
      <EuiSpacer size="xxl" />
      <Lottie config={lottieOptions} height="300px" />
      <EuiTitle size="l">
        <h1 style={{ textAlign: "center" }}>Full secure login</h1>
      </EuiTitle>
      <EuiSpacer size="xxl" />
      <EuiFlexGroup justifyContent="spaceAround">
        <EuiButton data-testid="login-btn" onClick={login}>
          Log in
        </EuiButton>
      </EuiFlexGroup>
      <EuiSpacer size="xxl" />
    </>
  );
}
