import React from "react";
import ItemsList from "../components/ItemsList";
import Search from "../components/Search";
import SearchArchieve from "../components/SearchArchieve";
import SaveSearchButton from "../components/SaveSearchButton";
import { EuiFlexItem, EuiFlexGroup, EuiSpacer } from "@elastic/eui";

export default function Dashboard() {
  return (
    <>
      <EuiFlexGroup>
        <EuiFlexItem>
          <Search />
          <EuiSpacer size="m" />
          <EuiFlexGroup gutterSize="s">
            <SearchArchieve />
            <SaveSearchButton />
          </EuiFlexGroup>
          <EuiSpacer size="xl" />
        </EuiFlexItem>
      </EuiFlexGroup>
      <ItemsList />
    </>
  );
}
