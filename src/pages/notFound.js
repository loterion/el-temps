import React from "react";
import { EuiTitle, EuiSpacer } from "@elastic/eui";

export default function NotFound() {
  return (
    <div>
      <EuiSpacer size="xxxl" />
      <EuiTitle size="l">
        <h1>Oops! parece que la página no existe.</h1>
      </EuiTitle>
      <EuiSpacer size="xxxl" />
    </div>
  );
}
