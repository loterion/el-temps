import { call, put, select, all } from "redux-saga/effects";
import { actionCreators as itemsListActionCreators } from "../features/itemsList";
import { selector as selectedProvinciasSelector } from "../features/selectedProvincias";
import Api from "../api";

export function* getDetailMunicipio({ CODPROV, CODIGOINE }) {
  try {
    const idMunicipio = CODIGOINE.substring(0, 5);
    const data = yield call(Api.requestMunicipio, {
      codigoProvincia: CODPROV,
      codigoMunicipio: idMunicipio,
    });
    yield put(itemsListActionCreators.detailAddMunicipio(data));
  } catch (err) {
    yield put(itemsListActionCreators.detailFailed(err));
  }
}

function* getProvinciaDetail(data) {
  const { municipios } = data;
  yield put(itemsListActionCreators.detailAddProvincia(data));
  yield all(municipios.map((municipio) => call(getDetailMunicipio, municipio)));
}

const selectorProvincias = (state) => selectedProvinciasSelector(state);

export default function* tryList() {
  try {
    yield put(itemsListActionCreators.detailClean());
    const obj = yield select(selectorProvincias);
    const provincias = obj.selectedProvincias;
    for (const i in provincias) {
      if (provincias.hasOwnProperty(i)) {
        const data = yield call(Api.requestProvincia, provincias[i].codigo);
        yield getProvinciaDetail(data);
      }
    }
  } catch (err) {
    yield put(itemsListActionCreators.detailFailed(err));
  }
}
