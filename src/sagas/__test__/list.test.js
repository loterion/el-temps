import { actionCreators } from "../../features/itemsList";
import { getDetailMunicipio } from "../list";
import { runSaga } from "redux-saga";
import Api from "../../api";

describe("provincias saga and API request", () => {
  it("should call API and dispatch detailAddMunicipio", async () => {
    const sagaParams = {
      CODPROV: "01",
      CODIGOINE: "01001000000",
    };
    const dummyMunicipio = {
      municipio: {
        CODPROV: "01",
        CODIGOINE: "01001000000",
      },
    };
    const requestMunicipio = jest
      .spyOn(Api, "requestMunicipio")
      .mockImplementation(() => Promise.resolve(dummyMunicipio));
    const dispatched = [];
    const result = await runSaga(
      {
        dispatch: (action) => dispatched.push(action),
      },
      getDetailMunicipio,
      sagaParams
    );

    expect(requestMunicipio).toHaveBeenCalledTimes(1);
    expect(dispatched).toEqual([
      actionCreators.detailAddMunicipio(dummyMunicipio),
    ]);
    requestMunicipio.mockClear();
  });
});
