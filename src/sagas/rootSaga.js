import { takeLatest } from "redux-saga/effects";
import tryList from "./list";

import { actionTypes as listActions } from "../features/itemsList";

export default function* rootSaga() {
  yield takeLatest(listActions.DETAIL_TRY, tryList);
}
