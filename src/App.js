import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import Layout from "./components/Layout";
import Dashboard from "./pages/dashboard";
import Login from "./pages/login";
import NotFound from "./pages/notFound";

export const fakeAuth = {
  isAuthenticated: false,
  setIsAuthenticated(b) {
    this.isAuthenticated = b;
  },
  authenticate(cb) {
    this.isAuthenticated = true;
    setTimeout(cb, 100); //fake async
  },
  signout(cb) {
    this.isAuthenticated = false;
    setTimeout(cb, 100); //fake async
  },
};

function PrivateRoute({ children, ...rest }) {
  return (
    <Route
      {...rest}
      render={() => {
        return fakeAuth.isAuthenticated ? children : <Redirect to="/login" />;
      }}
    />
  );
}

function App() {
  return (
    <Router>
      <div className="App">
        <Layout>
          <Switch>
            <Route path="/login">
              <Login fakeAuth={fakeAuth} />
            </Route>
            <PrivateRoute path="/">
              <Dashboard />
            </PrivateRoute>
            <Route path="*" component={NotFound} />
          </Switch>
        </Layout>
      </div>
    </Router>
  );
}

export default App;
