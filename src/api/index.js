function requestProvincia(codigoProvincia) {
  console.log(codigoProvincia);
  const URL = `https://www.el-tiempo.net/api/json/v2/provincias/${codigoProvincia}/municipios`;
  return fetch(URL)
    .then((response) => response.json())
    .then((data) => data);
}

function requestMunicipio({ codigoProvincia, codigoMunicipio }) {
  const URL = `https://www.el-tiempo.net/api/json/v2/provincias/${codigoProvincia}/municipios/${codigoMunicipio}`;
  return fetch(URL)
    .then((response) => response.json())
    .then((data) => data);
}

const api = {
  requestProvincia,
  requestMunicipio,
};

export default api;
