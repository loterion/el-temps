import React, { useState } from "react";
import {
  EuiFlexGroup,
  EuiFlexItem,
  EuiComboBox,
  EuiFormRow,
  EuiButton,
} from "@elastic/eui";
import municipios from "../../data/municipios";
import { useDispatch } from "react-redux";
import { actionCreators as selectedProvinciasActions } from "../../features/selectedProvincias";
import { actionCreators as itemsListActions } from "../../features/itemsList";

export default function Search() {
  const [selectedOptions, setSelected] = useState([]);
  const [error, setError] = useState(undefined);
  const [inputRef, setInputRef] = useState(undefined);
  const dispatch = useDispatch();

  //Update combo state and update selected options in the global state
  const onChange = (selectedOptions) => {
    setSelected(selectedOptions);
    dispatch(selectedProvinciasActions.setProvincias(selectedOptions));
    setError(undefined);
  };

  const onSearchChange = (value, hasMatchingOptions) => {
    setError(
      value.length === 0 || hasMatchingOptions
        ? undefined
        : `"${value}" no es un municipio válido`
    );
  };

  const onBlur = () => {
    if (inputRef) {
      const { value } = inputRef;
      setError(
        value.length === 0 ? undefined : `"${value}" no es un municipio válido`
      );
    }
  };

  return (
    <EuiFlexGroup>
      <EuiFlexItem grow={10}>
        <EuiFormRow error={error} fullWidth isInvalid={error !== undefined}>
          <EuiComboBox
            placeholder="Elige un municipio"
            options={municipios}
            selectedOptions={selectedOptions}
            inputRef={setInputRef}
            onChange={onChange}
            onSearchChange={onSearchChange}
            onBlur={onBlur}
            fullWidth
            data-testid="combo-field"
          />
        </EuiFormRow>
      </EuiFlexItem>
      <EuiFlexItem grow={2}>
        <EuiButton
          data-testid="search-btn"
          onClick={() => dispatch(itemsListActions.list("08"))}
          fill
        >
          Buscar
        </EuiButton>
      </EuiFlexItem>
    </EuiFlexGroup>
  );
}
