import React from "react";
import Search from "../";
import { render, screen, fireEvent } from "@testing-library/react";
import ReduxProvider from "../../../store/ReduxProvider";

describe("Search component", () => {
  it("Display error message when it can not find the typed provincia", () => {
    render(
      <ReduxProvider>
        <Search />
      </ReduxProvider>,
      { initialState: [] }
    );
    const combo = screen.getByTestId("combo-field");
    expect(combo).toBeInTheDocument();
    const searchBtn = screen.getByTestId("search-btn");
    combo.setAttribute("selectedOptions", [{ label: "invalid option" }]);
    fireEvent.click(searchBtn);
  });
});
