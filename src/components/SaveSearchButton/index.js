import React from "react";
import { EuiFlexItem, EuiButton } from "@elastic/eui";
import { useDispatch, useSelector } from "react-redux";
import { actionCreators as historyActions } from "../../features/historyList";
import { selector as listSelector } from "../../features/itemsList";

export default function SaveSearchButton() {
  const dispatch = useDispatch();
  const { listState } = useSelector((state) => listSelector(state));

  //Setup object history
  const date = new Date().toLocaleString();
  const historyItem = {
    date: date,
    id: Math.floor(Math.random() * 500),
    detailList: listState.detailList,
  };

  return (
    <EuiFlexItem grow={false}>
      <EuiButton
        size="s"
        onClick={() => dispatch(historyActions.historyAdd(historyItem))}
        fill
      >
        Guardar busqueda
      </EuiButton>
    </EuiFlexItem>
  );
}
