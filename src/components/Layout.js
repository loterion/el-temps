import React from "react";
import PropTypes from "prop-types";
import Header from "./Header";
import { EuiPage, EuiPageBody, EuiPageContent, EuiSpacer } from "@elastic/eui";
import "@elastic/eui/dist/eui_theme_light.css";

export default function Layout({ children }) {
  return (
    <EuiPage restrictWidth={true}>
      <EuiPageBody component="div">
        <Header title="El tiempo" />
        <EuiSpacer size="m" />
        <EuiPageContent>{children}</EuiPageContent>
      </EuiPageBody>
    </EuiPage>
  );
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};
