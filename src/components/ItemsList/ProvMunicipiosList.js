import React from "react";
import PropTypes from "prop-types";
import Item from "../Item";
import { EuiFlexGrid, EuiTitle, EuiSpacer } from "@elastic/eui";

export default function ProvMunicipiosList({ data }) {
  const { title, municipios } = data;

  return (
    <div>
      <EuiSpacer size="xxl" />
      <EuiTitle size="l">
        <h1 data-testid="provincia-title" style={{ textAlign: "center" }}>
          {title}
        </h1>
      </EuiTitle>
      <EuiSpacer size="s" />
      <EuiFlexGrid data-testid="wrapper-municipios" columns={3}>
        {municipios.map((item, index) => (
          <Item key={item.municipio.CODIGOINE} data={item} />
        ))}
      </EuiFlexGrid>
    </div>
  );
}

ProvMunicipiosList.propTypes = {
  data: PropTypes.object,
};
