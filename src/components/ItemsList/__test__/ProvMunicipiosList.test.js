import React from "react";
import ProvMunicipiosList from "../ProvMunicipiosList";
import { screen } from "@testing-library/react";
import { renderWithRedux as render } from "../../../testUtils";

const dummyData = {
  title: "dummy title",
  municipios: [
    {
      stateSky: { description: "dummy description" },
      municipio: { CODIGOINE: "01", NOMBRE: "dummy municipio 01" },
    },
    {
      stateSky: { description: "dummy description" },
      municipio: { CODIGOINE: "02", NOMBRE: "dummy municipio 02" },
    },
  ],
};

function reducer(state, action) {
  switch (action.type) {
    default:
      return state;
  }
}

describe("ProvMunicipiosList component", () => {
  it("should render without errors", () => {});
  render(<ProvMunicipiosList data={dummyData} />, reducer);
  expect(screen.getByTestId("provincia-title")).toHaveTextContent(
    dummyData.title
  );
  expect(screen.getByTestId("wrapper-municipios")).not.toBeEmptyDOMElement();
});
