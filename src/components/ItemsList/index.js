import React, { useEffect } from "react";
import ProvMunicipiosList from "./ProvMunicipiosList";
import { EuiFlexGrid } from "@elastic/eui";
import { useSelector, useDispatch } from "react-redux";
import {
  actionCreators as listActions,
  selector as listSelector,
} from "../../features/itemsList";

export default function ItemsList() {
  const dispatch = useDispatch();
  const { listState } = useSelector((state) => listSelector(state));

  useEffect(() => {
    dispatch(listActions.list());
  }, [dispatch]);

  const provincias = listState.detailList;

  return (
    <div>
      <EuiFlexGrid columns={3}>
        {provincias &&
          provincias.map((provincia, index) => (
            <ProvMunicipiosList key={provincia.codprov} data={provincia} />
          ))}
      </EuiFlexGrid>
    </div>
  );
}
