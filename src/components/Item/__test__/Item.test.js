import React from "react";
import Item from "../";
import { screen, fireEvent, cleanup } from "@testing-library/react";
import { actionCreators } from "../../../features/itemsList";
import { renderWithRedux as render } from "../../../testUtils";

jest.mock("react-redux", () => {
  const { Provider, connect } = jest.requireActual("react-redux");
  return {
    useDispatch: () => jest.fn(),
    Provider,
    connect,
  };
});

const startingState = {
  detailList: [
    {
      codprov: "01",
      provincia: "Álava",
      municipios: [
        {
          municipio: {
            CODPROV: "01",
            CODIGOINE: "",
          },
        },
      ],
    },
  ],
};

const dummyItemData = {
  municipio: {
    NOMBRE: "dummy item",
  },
  temperatura_actual: "00",
  stateSky: {
    description: "dummy description",
  },
};

function reducer(state = startingState, action) {
  switch (action.type) {
    case "DETAIL_DELETE_MUNICIPIO":
      return { ...state, municipios: [] };
    default:
      return state;
  }
}

afterEach(cleanup);

describe("Item component", () => {
  it("should render with redux", () => {
    render(<Item data={dummyItemData} />, reducer);
    expect(screen.getByTestId("item-title")).toHaveTextContent(
      dummyItemData.municipio.NOMBRE
    );
    expect(screen.getByTestId("item-temperatura")).toHaveTextContent(
      dummyItemData.temperatura_actual
    );
    expect(screen.getByTestId("item-description")).toHaveTextContent(
      dummyItemData.stateSky.description
    );
  });

  it("should dispatch DETAIL_DELETE_MUNICIPIO", () => {
    render(<Item data={dummyItemData} />, reducer);
    jest.spyOn(actionCreators, "detailDeleteMunicipio");
    fireEvent.click(screen.getByTestId("item-eliminar-btn"));
    expect(actionCreators.detailDeleteMunicipio).toHaveBeenCalledTimes(1);
    jest.clearAllMocks();
  });
});
