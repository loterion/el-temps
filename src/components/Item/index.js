import React from "react";
import PropTypes from "prop-types";
import { EuiFlexItem, EuiCard, EuiButtonEmpty } from "@elastic/eui";
import { useDispatch } from "react-redux";
import { actionCreators as listActions } from "../../features/itemsList";
import WeatherIcon from "../weatherIcon";

export default function Item({ data }) {
  const dispatch = useDispatch();

  return (
    <EuiFlexItem>
      <EuiCard
        icon={<WeatherIcon str={data.stateSky.description} />}
        title={
          <span data-testid="item-title">{`${data.municipio.NOMBRE}`}</span>
        }
        description={
          <span>
            <span data-testid="item-temperatura">{`Temperatura actual: ${data.temperatura_actual}°C`}</span>
            <br />
            <span data-testid="item-description">
              {data.stateSky.description}
            </span>
          </span>
        }
        footer={
          <div>
            <EuiButtonEmpty
              color="danger"
              size="xs"
              aria-label="Eliminar item de la lista"
              data-testid="item-eliminar-btn"
              onClick={() => dispatch(listActions.detailDeleteMunicipio(data))}
            >
              Eliminar
            </EuiButtonEmpty>
          </div>
        }
      />
    </EuiFlexItem>
  );
}

Item.propTypes = {
  data: PropTypes.object,
};
