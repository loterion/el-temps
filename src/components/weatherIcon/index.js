import React from "react";
import PropTypes from "prop-types";
import { Lottie } from "@crello/react-lottie";
import nublado from "../../lottie/nublado.json";
import soleado from "../../lottie/soleado.json";
import nieve from "../../lottie/nieve.json";
import niebla from "../../lottie/niebla.json";
import tormenta from "../../lottie/tormenta.json";
import viento from "../../lottie/viento.json";

export default function WeatherIcon({ str }) {
  let ico = soleado;

  if (str.includes("Muy nuboso") || str.includes("Cubierto")) {
    ico = viento;
  } else if (
    str.includes("nuboso") ||
    str.includes("Nuboso") ||
    str.includes("Nuves altas")
  ) {
    ico = nublado;
  } else if (str.includes("nieve")) {
    ico = nieve;
  } else if (str.includes("viento")) {
    ico = viento;
  } else if (str.includes("lluvia") || str.includes("tormenta")) {
    ico = tormenta;
  } else if (str.includes("niebla") || str.includes("neblina")) {
    ico = niebla;
  }

  const lottieOptions = {
    loop: true,
    autoplay: true,
    animationData: ico,
    renderSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return <Lottie config={lottieOptions} height={100} />;
}

WeatherIcon.propTypes = {
  str: PropTypes.string,
};
