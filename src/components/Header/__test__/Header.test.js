import React from "react";
import { render, screen } from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";
import Header from "../index.js";

describe("Header", () => {
  it("has title", () => {
    render(
      <Router>
        <Header />
      </Router>
    );
    const result = screen.getAllByTestId("header-component");
    expect(result).not.toBe("");
  });
});
