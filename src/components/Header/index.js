import React from "react";
import { EuiHeader, EuiHeaderSectionItem, EuiHeaderLogo } from "@elastic/eui";
import { Link } from "react-router-dom";

export default function Header({ title }) {
  return (
    <EuiHeader>
      <EuiHeaderSectionItem border="none">
        <EuiHeaderLogo data-testid="header-component">
          {title || "Title"}
        </EuiHeaderLogo>
      </EuiHeaderSectionItem>

      <EuiHeaderSectionItem border="none">
        <Link style={{ marginRight: "2rem" }} to="/dashboard">
          Dashboard
        </Link>
        <Link style={{ marginRight: "2rem" }} to="/login">
          Login
        </Link>
      </EuiHeaderSectionItem>
    </EuiHeader>
  );
}
