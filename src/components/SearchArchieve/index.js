import React, { useState } from "react";
import SearchArchieveTable from "../SearchArchieveTable";
import {
  EuiFlexItem,
  EuiFlyout,
  EuiFlyoutHeader,
  EuiFlyoutBody,
  EuiButton,
  EuiTitle,
} from "@elastic/eui";

export default function SearchArchieve() {
  const [isVisible, setVisible] = useState(false);
  const closeSearchArchieve = () => setVisible(false);
  const showSearchArchieve = () => setVisible(true);

  let flyout;
  if (isVisible) {
    flyout = (
      <EuiFlyout
        onClose={closeSearchArchieve}
        size="s"
        aria-labelledby="flyoutSmallTitle"
      >
        <EuiFlyoutHeader hasBorder>
          <EuiTitle size="s">
            <h2 id="flyoutSmallTitle">Tus busquedas guardadas</h2>
          </EuiTitle>
        </EuiFlyoutHeader>
        <EuiFlyoutBody>
          <SearchArchieveTable />
        </EuiFlyoutBody>
      </EuiFlyout>
    );
  }

  return (
    <EuiFlexItem grow={false}>
      <EuiButton size="s" onClick={showSearchArchieve}>
        Busquedas guardadas
      </EuiButton>
      {flyout}
    </EuiFlexItem>
  );
}
