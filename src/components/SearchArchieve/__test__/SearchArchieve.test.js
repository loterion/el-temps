import React from "react";
import SearchArchieve from "../";
import { screen, fireEvent } from "@testing-library/react";
import { renderWithRedux as render } from "../../../testUtils";

const initialState = {
  history: {
    historyList: [],
  },
};

function reducer(state, action) {
  switch (action.type) {
    default:
      return state;
  }
}

describe("SearchArchieve component", () => {
  it("should render without errors", () => {
    render(<SearchArchieve />, reducer);
    expect(
      screen.queryByText(/tus busquedas guardadas/i)
    ).not.toBeInTheDocument();
  });

  it("should open the flyout", () => {
    render(<SearchArchieve />, reducer, { initialState });
    const btn = screen.getByText(/busquedas guardadas/i);
    fireEvent.click(btn);
    expect(screen.getByText(/tus busquedas guardadas/i)).toBeInTheDocument();
  });
});
