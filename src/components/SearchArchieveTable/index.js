import React from "react";
import { EuiBasicTable } from "@elastic/eui";
import { useSelector } from "react-redux";
import { selector as historySelector } from "../../features/historyList";

export default function SearchArchieveTable() {
  const { historyState } = useSelector((state) => historySelector(state));

  const columns = [
    {
      field: "date",
      name: "Fecha",
      sortable: false,
      "data-test-subj": "dateCell",
      mobileOptions: {
        render: (item) => <span>{item.date}</span>,
        header: false,
        truncateText: false,
        enlarge: true,
        fullWidth: true,
      },
    },
  ];

  const getRowProps = (item) => {
    const { id } = item;
    return {
      "data-test-subj": `row-${id}`,
      className: "customRowClass",
      onClick: () => {},
    };
  };

  const getCellProps = (item, column) => {
    const { id } = item;
    const { field } = column;
    return {
      className: "customCellClass",
      "data-test-subj": `cell-${id}-${field}`,
      textOnly: true,
    };
  };

  return (
    <EuiBasicTable
      items={historyState.historyList}
      rowHeader="date"
      columns={columns}
      rowProps={getRowProps}
      cellProps={getCellProps}
    />
  );
}
