import { combineReducers } from "redux";
import itemsListReducer, { NAME as itemsListName } from "../features/itemsList";
import selectedProvinciasReducer, {
  NAME as selectedProvinciasName,
} from "../features/selectedProvincias";
import historyListReducer, {
  NAME as historyListName,
} from "../features/historyList";

export default function createRootReducer() {
  return combineReducers({
    [itemsListName]: itemsListReducer,
    [historyListName]: historyListReducer,
    [selectedProvinciasName]: selectedProvinciasReducer,
  });
}
