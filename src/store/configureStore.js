import { createStore, applyMiddleware, compose } from "redux";
import createRootReducer from "./reducers";
import createSagaMiddleWare from "redux-saga";
import rootSaga from "../sagas/rootSaga";
import storage from "redux-persist/lib/storage";
import { persistStore, persistReducer } from "redux-persist";

const persistConfig = {
  key: "root",
  storage,
};

const persistedReducer = persistReducer(persistConfig, createRootReducer());

export default function configStore({ initialState }) {
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const sagaMiddleWare = createSagaMiddleWare();
  const store = createStore(
    persistedReducer,
    composeEnhancers(applyMiddleware(sagaMiddleWare))
  );
  const persistor = persistStore(store);
  sagaMiddleWare.run(rootSaga);
  return { store, persistor };
}
