import configureStore from "./configureStore";

const initialState = {};

let store = null;
let persistor = null;

export default function getStore() {
  if (store === null) {
    const result = configureStore({ initialState });
    store = result.store;
    persistor = result.persistor;
  }
  return { store, persistor };
}
