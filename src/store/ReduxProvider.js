import React from "react";
import { Provider } from "react-redux";
import getStore from "./getStore";
import { PersistGate } from "redux-persist/integration/react";

const { store, persistor } = getStore();

export default function ReduxProvider({ children }) {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        {children}
      </PersistGate>
    </Provider>
  );
}
